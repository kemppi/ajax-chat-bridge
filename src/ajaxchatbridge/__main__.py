import argparse
import json
import logging
from ajaxchatbridge import util
from ajaxchatbridge.session import Session


def main():
    logging.basicConfig(level=logging.DEBUG)

    p = argparse.ArgumentParser(prog="ajaxchatbridge")
    p.add_argument("config_file", type=str)
    args = p.parse_args()

    with open(args.config_file) as fp:
        config = json.load(fp)

    urls = util.urls(config["urls"]["forum"], config["urls"]["chat"])

    session = Session(urls)
    try:
        session.login(config["user"], config["pass"])
    finally:
        session.logout()

if __name__ == "__main__":
    main()
