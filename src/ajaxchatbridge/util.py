

def urls(forum_url, chat_url):
    url_dict = {
        "login": forum_url + "ucp.php?mode=login",
        "logout": forum_url + "ucp.php?mode=logout&sid=%(sid)s",
        "info": chat_url + "?ajax=true&lastID=0&getInfos=userID%2CuserName%2CuserRole%2CchannelID%2CchannelName&channelID=0"
    }
    return url_dict
