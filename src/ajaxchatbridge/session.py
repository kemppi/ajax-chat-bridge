from urllib import urlencode
import urllib2
import cookielib
import logging

_log = logging.getLogger(__name__)

PHPBB_SID_COOKIE = lambda c: c.name.startswith("phpbb3_") and c.name.endswith("_sid")
PHPBB_USER_COOKIE = lambda name: name.startswith("phpbb3_") and name.endswith("_u")
PHPBB_USER_COOKIE_LOGGED_IN = lambda value: value not in ("", "1")
PHPBB_USER_COOKIE_LOGGED_OUT = lambda value: not PHPBB_USER_COOKIE_LOGGED_IN(value)


class Session(object):

    def __init__(self, urls):
        self._urls = urls
        self._cj = cookielib.CookieJar()
        self._opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self._cj))
        self._auth_ok = False
        assert isinstance(self._opener, urllib2.OpenerDirector)

    def login(self, username, password):
        self._opener.open(self._urls["login"], data=urlencode({
            "username": username,
            "password": password,
            "redirect": "",
            "login": ""
        }))

        user_cookie = filter(lambda c: PHPBB_USER_COOKIE(c.name) and PHPBB_USER_COOKIE_LOGGED_IN(c.value), self._cj)
        if not user_cookie:
            raise Exception("Login failed!")
        else:
            user_cookie, = user_cookie
            self._auth_ok = True

        _log.debug("Logged in as uid %s" % user_cookie.value)

    def logout(self):
        if not self._auth_ok:
            return

        # TODO: chat logout
        sid = filter(PHPBB_SID_COOKIE, self._cj)[0].value

        self._opener.open(self._urls["logout"] % {"sid": sid})
        if not filter(lambda c: PHPBB_USER_COOKIE(c.name) and PHPBB_USER_COOKIE_LOGGED_OUT(c.value), self._cj):
            _log.debug("Cookies after failed logout attempt:")
            for c in self._cj:
                _log.debug(c)
            raise Exception("Logout failed!")

        _log.debug("Logged out")

