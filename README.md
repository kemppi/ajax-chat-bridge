# AJAX Chat Bridge

This will be a bridge implementation for [Blueimp's AJAX Chat](http://frug.github.io/AJAX-Chat/). The purpose is to bypass the default client that runs on the web browser, and let other applications and protocols (e.g. IRC) interface with the chat in a way that is transparent (enough) to the chat server.

## Requirements

* python 2.7 (tested with 2.7.4)

## Usage

`python -m ajaxchatbridge config_file`
